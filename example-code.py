from greenbyteapi.greenbyteapi_client import GreenbyteapiClient
from datetime import datetime, timedelta
from scipy.stats import pearsonr
import sys
import math

def distance_between_coordinates(latitude1, longitude1, latitude2, longitude2):
    """
    Returns the distance between two lat long coordinates in kilometers.

    This calculation is taken from the following guide:
    https://stackoverflow.com/questions/27928/calculate-distance-between-two-latitude-longitude-points-haversine-formula
    """

    def deg2rad(degree):
        return degree * math.pi/180

    r = 6371; # Radius of the earth in km
    dLat = deg2rad(latitude2 - latitude1)
    dLon = deg2rad(longitude2 - longitude1); 
    a = math.sin(dLat/2) * math.sin(dLat/2) + \
        math.cos(deg2rad(latitude1)) * math.cos(deg2rad(latitude2)) * \
        math.sin(dLon/2) * math.sin(dLon/2) 
    c = 2 * math.atan2(math.sqrt(a), math.sqrt(1-a)); 
    d = r * c # Distance in km
    return d

def remove_none_by_index(list1, list2):
    zipped = [x for x in zip(list1, list2) if x[0] and x[1]]
    return (list(x) for x in zip(*zipped))

api_token = sys.argv[1]
greenbyte_client = GreenbyteapiClient(api_token)
greenbyte_client.config.customer = 'demo'

target_device_id = 1
turbine_device_type_id = 1
power_turbine_signal_id = 5

all_turbine_devices = greenbyte_client.assets.get_devices([turbine_device_type_id])

# get the site_id from the target device
site_id = next(d for d in all_turbine_devices if d.device_id == target_device_id).site.site_id

# get all the devices that are on the same site (this also includes the target device itself)
site_devices = { d.device_id: d for d in all_turbine_devices if d.site.site_id == site_id }

data_time_range_start = datetime.now() - timedelta(days=1)
data_time_range_end = datetime.now()

# get the power data for the devices
power_data = greenbyte_client.data.get_data(list(site_devices.keys()), [power_turbine_signal_id], data_time_range_start, data_time_range_end)

# add the power data to all devices (in the data response, aggregate_id corresponds to the device_id in this case)
for power in power_data:
    site_devices[power.aggregate_id].power_data = list(power.data.values())

# remove the target device from site_devices and store into into a device variable
target_device = site_devices.pop(target_device_id)

for i, k in enumerate(site_devices):
    # The power data may include None, so lets go ahead and remove values if any of the power data includes a None.
    power_data_1, power_data_2 = remove_none_by_index(target_device.power_data, site_devices[k].power_data)
    site_devices[k].correlation_with_target = pearsonr(power_data_1, power_data_2)
    site_devices[k].distance_to_target = distance_between_coordinates(
        float(site_devices[k].latitude), float(site_devices[k].longitude), 
        float(target_device.latitude), float(target_device.longitude)
    )

sorted_by_correlation = sorted(list(site_devices.values()), key=lambda x: x.correlation_with_target, reverse=True)
sorted_by_distance = sorted(list(site_devices.values()), key=lambda x: x.distance_to_target)

print('Highest correlating turbines:')
for i, d in enumerate(sorted_by_correlation):
    print(f"{i+1}: Turbine device id: {d.device_id}, Correlation: {d.correlation_with_target[0]}")

print('\nClosest turbines:')
for i, d in enumerate(sorted_by_distance):
    print(f"{i+1}: Turbine device id: {d.device_id}, Distance from target device: {d.distance_to_target} km")
