from greenbyteapi.greenbyteapi_client import GreenbyteapiClient
from greenbyteapi.configuration import Configuration
from greenbyteapi.models.resolution_enum import ResolutionEnum
from greenbyteapi.models.aggregate_mode_enum import AggregateModeEnum
from greenbyteapi.models.calculation_mode_enum import CalculationModeEnum
from greenbyteapi.exceptions.api_exception import APIException
from datetime import datetime, timezone
from scipy.stats import pearsonr
import sys
import math
import dateutil

api_token = sys.argv[1]

client = GreenbyteapiClient(api_token)
client.config.customer = 'demo'

turbine_id = 1
site_id = 1
turbine_device_id = 1

try:
    all_turbine_devices = client.assets.get_devices([turbine_device_id])
except APIException as e:
    print(e) 
    exit(1)

# get all the devices that are on the same site as the device and exclude the device itself
site_devices = [d for d in all_turbine_devices if d.site.site_id == site_id]
site_devices_ids = [d.device_id for d in site_devices if d.device_id != turbine_id]

end = datetime.now(timezone.utc)
start = end.replace(day=end.day-1)

power_turbine_id = 5

try:
    device_power_data = client.data.get_data([turbine_id], [power_turbine_id], start, end)[0]
except APIException as e: 
    print(e)
    exit(1)

try:
    site_devices_power_data = client.data.get_data(site_devices_ids, [power_turbine_id], start, end)
except APIException as e:
    print(e)
    exit(1)

device_power_data_values = list(device_power_data.data.values())
for d in site_devices_power_data:
    d.correlation = pearsonr(device_power_data_values, list(d.data.values()))

sorted_correlation = sorted(
    site_devices_power_data, key=lambda x: x.correlation, reverse=True)
print('\nHighest correlating turbines:\n')

for i, d in enumerate(sorted_correlation):
    print(f"{i+1}: Turbine id: {d.aggregate_id}, Correlation: {d.correlation[0]}")
