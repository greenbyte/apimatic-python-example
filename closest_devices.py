from greenbyteapi.greenbyteapi_client import GreenbyteapiClient
from greenbyteapi.exceptions.api_exception import APIException
import sys
import math

api_token = sys.argv[1]
client = GreenbyteapiClient(api_token)
client.config.customer = 'demo'

device_id = 248

all_devices = client.assets.get_devices()

# get the target device
try:
    device = next(d for d in all_devices if d.device_id == device_id)
except:
    print(f"device with id: {device_id} was not found")
    exit(0)

# get all the devices that are on the same site as the device and exclude the device itself
site_devices = [d for d in all_devices
                if d.site.site_id == device.site.site_id
                and d.device_id != device_id]

# add a property to the site devices that specified how for away the device is from the target device 
for d in site_devices:
    d.distance_to_device = math.sqrt(
        abs(float(d.longitude) - float(device.longitude))
        + abs(float(d.latitude) - float(device.latitude))
    )

sorted_by_device = sorted(site_devices, key=lambda x: x.distance_to_device)

print(f"\nClosest devices to {device.title}:\n")
for i, d in enumerate(sorted_by_device):
    print(f"{i+1}: Device: {d.title} with Device ID: {d.device_id} is located {d.distance_to_device} km away")
