# -*- coding: utf-8 -*-

"""
    greenbyteapi

    This file was automatically generated by APIMATIC v2.0 ( https://apimatic.io ).
"""

import greenbyteapi.models.client_configuration
import greenbyteapi.models.time_zone_configuration
import greenbyteapi.models.data_signal_configuration

class Configuration(object):

    """Implementation of the 'Configuration' model.

    Configuration data for a customer.

    Attributes:
        client (ClientConfiguration): General configuration data for a
            customer.
        time_zone (TimeZoneConfiguration): Time zone configuration data for a
            customer.
        data_signals (DataSignalConfiguration): Data signal configuration data
            for a customer. These only apply to wind devices.

    """

    # Create a mapping from Model property names to API property names
    _names = {
        "client":'client',
        "time_zone":'timeZone',
        "data_signals":'dataSignals'
    }

    def __init__(self,
                 client=None,
                 time_zone=None,
                 data_signals=None):
        """Constructor for the Configuration class"""

        # Initialize members of the class
        self.client = client
        self.time_zone = time_zone
        self.data_signals = data_signals


    @classmethod
    def from_dictionary(cls,
                        dictionary):
        """Creates an instance of this model from a dictionary

        Args:
            dictionary (dictionary): A dictionary representation of the object as
            obtained from the deserialization of the server's response. The keys
            MUST match property names in the API description.

        Returns:
            object: An instance of this structure class.

        """
        if dictionary is None:
            return None

        # Extract variables from the dictionary
        client = greenbyteapi.models.client_configuration.ClientConfiguration.from_dictionary(dictionary.get('client')) if dictionary.get('client') else None
        time_zone = greenbyteapi.models.time_zone_configuration.TimeZoneConfiguration.from_dictionary(dictionary.get('timeZone')) if dictionary.get('timeZone') else None
        data_signals = greenbyteapi.models.data_signal_configuration.DataSignalConfiguration.from_dictionary(dictionary.get('dataSignals')) if dictionary.get('dataSignals') else None

        # Return an object of this model
        return cls(client,
                   time_zone,
                   data_signals)


