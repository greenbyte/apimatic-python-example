# Getting started

This is the public API for the Greenbyte Platform.
# General notes regarding endpoints
* Endpoints that take `page` and `pageSize` parameters return a `Link` header as defined in [RFC 8288](https://tools.ietf.org/html/rfc8288).
* All endpoints can also be reached using the POST method, with a JSON request body instead of query parameters.


## How to Build


You must have Python ```2 >=2.7.9``` or Python ```3 >=3.4``` installed on your system to install and run this SDK. This SDK package depends on other Python packages like nose, jsonpickle etc. 
These dependencies are defined in the ```requirements.txt``` file that comes with the SDK.
To resolve these dependencies, you can use the PIP Dependency manager. Install it by following steps at [https://pip.pypa.io/en/stable/installing/](https://pip.pypa.io/en/stable/installing/).

Python and PIP executables should be defined in your PATH. Open command prompt and type ```pip --version```.
This should display the version of the PIP Dependency Manager installed if your installation was successful and the paths are properly defined.

* Using command line, navigate to the directory containing the generated files (including ```requirements.txt```) for the SDK.
* Run the command ```pip install -r requirements.txt```. This should install all the required dependencies.

![Building SDK - Step 1](https://apidocs.io/illustration/python?step=installDependencies&workspaceFolder=Greenbyte%20API-Python)


## How to Use

The following section explains how to use the Greenbyteapi SDK package in a new project.

### 1. Open Project in an IDE

Open up a Python IDE like PyCharm. The basic workflow presented here is also applicable if you prefer using a different editor or IDE.

![Open project in PyCharm - Step 1](https://apidocs.io/illustration/python?step=pyCharm)

Click on ```Open``` in PyCharm to browse to your generated SDK directory and then click ```OK```.

![Open project in PyCharm - Step 2](https://apidocs.io/illustration/python?step=openProject0&workspaceFolder=Greenbyte%20API-Python)     

The project files will be displayed in the side bar as follows:

![Open project in PyCharm - Step 3](https://apidocs.io/illustration/python?step=openProject1&workspaceFolder=Greenbyte%20API-Python&projectName=greenbyteapi)     

### 2. Add a new Test Project

Create a new directory by right clicking on the solution name as shown below:

![Add a new project in PyCharm - Step 1](https://apidocs.io/illustration/python?step=createDirectory&workspaceFolder=Greenbyte%20API-Python&projectName=greenbyteapi)

Name the directory as "test"

![Add a new project in PyCharm - Step 2](https://apidocs.io/illustration/python?step=nameDirectory)
   
Add a python file to this project with the name "testsdk"

![Add a new project in PyCharm - Step 3](https://apidocs.io/illustration/python?step=createFile&workspaceFolder=Greenbyte%20API-Python&projectName=greenbyteapi)

Name it "testsdk"

![Add a new project in PyCharm - Step 4](https://apidocs.io/illustration/python?step=nameFile)

In your python file you will be required to import the generated python library using the following code lines

```Python
from greenbyteapi.greenbyteapi_client import GreenbyteapiClient
```

![Add a new project in PyCharm - Step 4](https://apidocs.io/illustration/python?step=projectFiles&workspaceFolder=Greenbyte%20API-Python&libraryName=greenbyteapi.greenbyteapi_client&projectName=greenbyteapi&className=GreenbyteapiClient)

After this you can write code to instantiate an API client object, get a controller object and  make API calls. Sample code is given in the subsequent sections.

### 3. Run the Test Project

To run the file within your test project, right click on your Python file inside your Test project and click on ```Run```

![Run Test Project - Step 1](https://apidocs.io/illustration/python?step=runProject&workspaceFolder=Greenbyte%20API-Python&libraryName=greenbyteapi.greenbyteapi_client&projectName=greenbyteapi&className=GreenbyteapiClient)


## How to Test

You can test the generated SDK and the server with automatically generated test
cases. unittest is used as the testing framework and nose is used as the test
runner. You can run the tests as follows:

  1. From terminal/cmd navigate to the root directory of the SDK.
  2. Invoke ```pip install -r test-requirements.txt```
  3. Invoke ```nosetests```

## Initialization

### Authentication
In order to setup authentication and initialization of the API client, you need the following information.

| Parameter | Description |
|-----------|-------------|
| api_token | TODO: add a description |



API client can be initialized as following.

```python
# Configuration parameters and credentials
api_token = 'api_token'

client = GreenbyteapiClient(api_token)
```



# Class Reference

## <a name="list_of_controllers"></a>List of Controllers

* [DataController](#data_controller)
* [StatusController](#status_controller)
* [MetadataController](#metadata_controller)
* [AssetsController](#assets_controller)
* [AlertsController](#alerts_controller)

## <a name="data_controller"></a>![Class: ](https://apidocs.io/img/class.png ".DataController") DataController

### Get controller instance

An instance of the ``` DataController ``` class can be accessed from the API Client.

```python
 data_controller = client.data
```

### <a name="get_data_signals"></a>![Method: ](https://apidocs.io/img/method.png ".DataController.get_data_signals") get_data_signals

> Gets available data signals for one or more devices. This request can also be made using the POST method, with a JSON request body instead of query parameters.

```python
def get_data_signals(self,
                         device_ids)
```

#### Parameters

| Parameter | Tags | Description |
|-----------|------|-------------|
| deviceIds |  ``` Required ```  ``` Collection ```  | What devices to get data signals for. |



#### Example Usage

```python
device_ids_value = "[1,2,3]"
device_ids = json.loads(device_ids_value)

result = data_controller.get_data_signals(device_ids)

```

#### Errors

| Error Code | Error Description |
|------------|-------------------|
| 400 | The request cannot be fulfilled due to bad syntax. |
| 401 | One of the following:<br>* The request is missing a valid API key.<br>* The API key does not authorize access the requested data.<br> |
| 405 | The HTTP method is not allowed for the endpoint. |
| 429 | The API key has been used in too many requests in a given amount of time. |




### <a name="get_data"></a>![Method: ](https://apidocs.io/img/method.png ".DataController.get_data") get_data

> Gets data for multiple devices and data signals in the given resolution. This request can also be made using the POST method, with a JSON request body instead of query parameters.

```python
def get_data(self,
                 device_ids,
                 data_signal_ids,
                 timestamp_start=None,
                 timestamp_end=None,
                 resolution='10minute',
                 aggregate='device',
                 calculation=None)
```

#### Parameters

| Parameter | Tags | Description |
|-----------|------|-------------|
| deviceIds |  ``` Required ```  ``` Collection ```  | Which devices to get data for. |
| dataSignalIds |  ``` Required ```  ``` Collection ```  | Which data signals to get data for. |
| timestampStart |  ``` Optional ```  | The first timestamp to get data for. Timestamps ending with 'Z' are treated as UTC. Other timestamps are treated as being in the customer's timezone. |
| timestampEnd |  ``` Optional ```  | The last timestamp to get data for. Timestamps ending with 'Z' are treated as UTC. Other timestamps are treated as being in the customer's timezone. |
| resolution |  ``` Optional ```  ``` DefaultValue ```  | The desired data resolution. |
| aggregate |  ``` Optional ```  ``` DefaultValue ```  | How the data should be aggregated with regards to device(s) or site(s). |
| calculation |  ``` Optional ```  | The calculation used when aggregating data, both over time and across devices. The default is the data signal default. |



#### Example Usage

```python
device_ids_value = "[1,2,3]"
device_ids = json.loads(device_ids_value)
data_signal_ids_value = "[1,5]"
data_signal_ids = json.loads(data_signal_ids_value)
timestamp_start = 2020-01-01T00:00:00.000Z
timestamp_end = 2020-01-08T00:00:00
resolution = ResolutionEnum.ENUM_10MINUTE
aggregate = AggregateModeEnum.DEVICE
calculation = CalculationModeEnum.SUM

result = data_controller.get_data(device_ids, data_signal_ids, timestamp_start, timestamp_end, resolution, aggregate, calculation)

```

#### Errors

| Error Code | Error Description |
|------------|-------------------|
| 400 | The request cannot be fulfilled due to bad syntax. |
| 401 | One of the following:<br>* The request is missing a valid API key.<br>* The API key does not authorize access the requested data.<br> |
| 405 | The HTTP method is not allowed for the endpoint. |
| 429 | The API key has been used in too many requests in a given amount of time. |




### <a name="get_real_time_data"></a>![Method: ](https://apidocs.io/img/method.png ".DataController.get_real_time_data") get_real_time_data

> Gets the most recent high-resolution data point for each specified device and data signal. This request can also be made using the POST method, with a JSON request body instead of query parameters.

```python
def get_real_time_data(self,
                           device_ids,
                           data_signal_ids,
                           aggregate='device',
                           calculation=None)
```

#### Parameters

| Parameter | Tags | Description |
|-----------|------|-------------|
| deviceIds |  ``` Required ```  ``` Collection ```  | Which devices to get data for. |
| dataSignalIds |  ``` Required ```  ``` Collection ```  | Which data signals to get data for. |
| aggregate |  ``` Optional ```  ``` DefaultValue ```  | How the data should be aggregated with regards to device(s) or site(s). |
| calculation |  ``` Optional ```  | The calculation used when aggregating data, both over time and across devices. The default is the data signal default. |



#### Example Usage

```python
device_ids_value = "[1,2,3]"
device_ids = json.loads(device_ids_value)
data_signal_ids_value = "[1,5]"
data_signal_ids = json.loads(data_signal_ids_value)
aggregate = AggregateModeEnum.DEVICE
calculation = CalculationModeEnum.SUM

result = data_controller.get_real_time_data(device_ids, data_signal_ids, aggregate, calculation)

```

#### Errors

| Error Code | Error Description |
|------------|-------------------|
| 400 | The request cannot be fulfilled due to bad syntax. |
| 401 | One of the following:<br>* The request is missing a valid API key.<br>* The API key does not authorize access the requested data.<br> |
| 405 | The HTTP method is not allowed for the endpoint. |
| 429 | The API key has been used in too many requests in a given amount of time. |




### <a name="get_data_per_category"></a>![Method: ](https://apidocs.io/img/method.png ".DataController.get_data_per_category") get_data_per_category

> Gets signal data aggregated per availability contract category. This request can also be made using the POST method, with a JSON request body instead of query parameters.

```python
def get_data_per_category(self,
                              device_ids,
                              data_signal_id,
                              timestamp_start=None,
                              timestamp_end=None,
                              aggregate='device',
                              category=None)
```

#### Parameters

| Parameter | Tags | Description |
|-----------|------|-------------|
| deviceIds |  ``` Required ```  ``` Collection ```  | Which devices to get data for. |
| dataSignalId |  ``` Required ```  | Which signal to get data for; only Lost Production signals are supported at the moment. |
| timestampStart |  ``` Optional ```  | The first timestamp to get data for. Timestamps ending with 'Z' are treated as UTC. Other timestamps are treated as being in the customer's timezone. |
| timestampEnd |  ``` Optional ```  | The last timestamp to get data for. Timestamps ending with 'Z' are treated as UTC. Other timestamps are treated as being in the customer's timezone. |
| aggregate |  ``` Optional ```  ``` DefaultValue ```  | How the data should be aggregated with regards to device(s) or site(s). |
| category |  ``` Optional ```  ``` Collection ```  | Which status categories to include. By default all categories are included. |



#### Example Usage

```python
device_ids_value = "[1,2,3]"
device_ids = json.loads(device_ids_value)
data_signal_id = 248
timestamp_start = 2020-01-01T00:00:00.000Z
timestamp_end = 2020-01-08T00:00:00
aggregate = AggregateModeEnum.DEVICE
category = [StatusCategoryEnum.STOP]

result = data_controller.get_data_per_category(device_ids, data_signal_id, timestamp_start, timestamp_end, aggregate, category)

```

#### Errors

| Error Code | Error Description |
|------------|-------------------|
| 400 | The request cannot be fulfilled due to bad syntax. |
| 401 | One of the following:<br>* The request is missing a valid API key.<br>* The API key does not authorize access the requested data.<br> |
| 405 | The HTTP method is not allowed for the endpoint. |
| 429 | The API key has been used in too many requests in a given amount of time. |




[Back to List of Controllers](#list_of_controllers)

## <a name="status_controller"></a>![Class: ](https://apidocs.io/img/class.png ".StatusController") StatusController

### Get controller instance

An instance of the ``` StatusController ``` class can be accessed from the API Client.

```python
 status_controller = client.status
```

### <a name="get_statuses"></a>![Method: ](https://apidocs.io/img/method.png ".StatusController.get_statuses") get_statuses

> Gets statuses for multiple devices during the given time period. This request can also be made using the POST method, with a JSON request body instead of query parameters.

```python
def get_statuses(self,
                     device_ids,
                     timestamp_start=None,
                     timestamp_end=None,
                     category=None,
                     fields=None,
                     sort_by=None,
                     sort_asc=False,
                     page_size='50',
                     page='1')
```

#### Parameters

| Parameter | Tags | Description |
|-----------|------|-------------|
| deviceIds |  ``` Required ```  ``` Collection ```  | Which devices to get statuses for. |
| timestampStart |  ``` Optional ```  | The first timestamp to get data for. Timestamps ending with 'Z' are treated as UTC. Other timestamps are treated as being in the customer's timezone. |
| timestampEnd |  ``` Optional ```  | The last timestamp to get data for. Timestamps ending with 'Z' are treated as UTC. Other timestamps are treated as being in the customer's timezone. |
| category |  ``` Optional ```  ``` Collection ```  | Which status categories to get statuses for. |
| fields |  ``` Optional ```  ``` Collection ```  | Which fields to include in the response. Valid fields are those defined in the StatusItem schema. By default all fields are included. |
| sortBy |  ``` Optional ```  ``` Collection ```  | Which fields to sort the response items by. By default the items are sorted by timestampStart. |
| sortAsc |  ``` Optional ```  ``` DefaultValue ```  | Whether to sort the items in ascending order. |
| pageSize |  ``` Optional ```  ``` DefaultValue ```  | The number of items to return per page. |
| page |  ``` Optional ```  ``` DefaultValue ```  | Which page to return when the number of items exceed the page size. |



#### Example Usage

```python
device_ids_value = "[1,2,3]"
device_ids = json.loads(device_ids_value)
timestamp_start = 2020-01-01T00:00:00.000Z
timestamp_end = 2020-01-08T00:00:00
category = [StatusCategoryEnum.STOP]
fields_value = '["deviceId","message","lostProduction"]'
fields = json.loads(fields_value)
sort_by = ['sortBy']
sort_asc = False
page_size = '50'
page = '1'

result = status_controller.get_statuses(device_ids, timestamp_start, timestamp_end, category, fields, sort_by, sort_asc, page_size, page)

```


### <a name="get_active_statuses"></a>![Method: ](https://apidocs.io/img/method.png ".StatusController.get_active_statuses") get_active_statuses

> Gets active statuses for multiple devices. This request can also be made using the POST method, with a JSON request body instead of query parameters.

```python
def get_active_statuses(self,
                            device_ids,
                            category=None,
                            fields=None,
                            sort_by=None,
                            sort_asc=False,
                            page_size='50',
                            page='1')
```

#### Parameters

| Parameter | Tags | Description |
|-----------|------|-------------|
| deviceIds |  ``` Required ```  ``` Collection ```  | Which devices to get statuses for. |
| category |  ``` Optional ```  ``` Collection ```  | Which status categories to get statuses for. |
| fields |  ``` Optional ```  ``` Collection ```  | Which fields to include in the response. Valid fields are those defined in the StatusItem schema. By default all fields are included. |
| sortBy |  ``` Optional ```  ``` Collection ```  | Which fields to sort the response items by. By default the items are sorted by timestampStart. |
| sortAsc |  ``` Optional ```  ``` DefaultValue ```  | Whether to sort the items in ascending order. |
| pageSize |  ``` Optional ```  ``` DefaultValue ```  | The number of items to return per page. |
| page |  ``` Optional ```  ``` DefaultValue ```  | Which page to return when the number of items exceed the page size. |



#### Example Usage

```python
device_ids_value = "[1,2,3]"
device_ids = json.loads(device_ids_value)
category = [StatusCategoryEnum.STOP]
fields_value = '["deviceId","message","lostProduction"]'
fields = json.loads(fields_value)
sort_by = ['sortBy']
sort_asc = False
page_size = '50'
page = '1'

result = status_controller.get_active_statuses(device_ids, category, fields, sort_by, sort_asc, page_size, page)

```


[Back to List of Controllers](#list_of_controllers)

## <a name="metadata_controller"></a>![Class: ](https://apidocs.io/img/class.png ".MetadataController") MetadataController

### Get controller instance

An instance of the ``` MetadataController ``` class can be accessed from the API Client.

```python
 metadata_controller = client.metadata
```

### <a name="get_configuration"></a>![Method: ](https://apidocs.io/img/method.png ".MetadataController.get_configuration") get_configuration

> Gets configuration data for a customer. This request can also be made using the POST method, with a JSON request body instead of query parameters.

```python
def get_configuration(self)
```

#### Example Usage

```python

result = metadata_controller.get_configuration()

```


[Back to List of Controllers](#list_of_controllers)

## <a name="assets_controller"></a>![Class: ](https://apidocs.io/img/class.png ".AssetsController") AssetsController

### Get controller instance

An instance of the ``` AssetsController ``` class can be accessed from the API Client.

```python
 assets_controller = client.assets
```

### <a name="get_devices"></a>![Method: ](https://apidocs.io/img/method.png ".AssetsController.get_devices") get_devices

> Gets a list of devices that the API key has permissions for. This request can also be made using the POST method, with a JSON request body instead of query parameters.

```python
def get_devices(self,
                    device_type_ids=None,
                    fields=None,
                    page_size='50',
                    page='1')
```

#### Parameters

| Parameter | Tags | Description |
|-----------|------|-------------|
| deviceTypeIds |  ``` Optional ```  ``` Collection ```  | Which device types to get. |
| fields |  ``` Optional ```  ``` Collection ```  | Which fields to include in the response. Valid fields are those defined in the Device schema. By default all fields are included. |
| pageSize |  ``` Optional ```  ``` DefaultValue ```  | The number of items to return per page. |
| page |  ``` Optional ```  ``` DefaultValue ```  | Which page to return when the number of items exceed the page size. |



#### Example Usage

```python
device_type_ids_value = "[1,2,3]"
device_type_ids = json.loads(device_type_ids_value)
fields = ['fields']
page_size = '50'
page = '1'

result = assets_controller.get_devices(device_type_ids, fields, page_size, page)

```


### <a name="get_power_curves"></a>![Method: ](https://apidocs.io/img/method.png ".AssetsController.get_power_curves") get_power_curves

> Gets the default or learned power curves for wind turbines. Other device types are not supported. This request can also be made using the POST method, with a JSON request body instead of query parameters.

```python
def get_power_curves(self,
                         device_ids,
                         timestamp=None,
                         learned=False)
```

#### Parameters

| Parameter | Tags | Description |
|-----------|------|-------------|
| deviceIds |  ``` Required ```  ``` Collection ```  | What devices to get power curves for. Only wind turbines are supported. |
| timestamp |  ``` Optional ```  | The date for which to get power curves. The default is the current date. |
| learned |  ``` Optional ```  ``` DefaultValue ```  | Whether to get learned power curves instead of default power curves. |



#### Example Usage

```python
device_ids_value = "[1,2,3]"
device_ids = json.loads(device_ids_value)
timestamp = 2020-01-01
learned = False

result = assets_controller.get_power_curves(device_ids, timestamp, learned)

```


[Back to List of Controllers](#list_of_controllers)

## <a name="alerts_controller"></a>![Class: ](https://apidocs.io/img/class.png ".AlertsController") AlertsController

### Get controller instance

An instance of the ``` AlertsController ``` class can be accessed from the API Client.

```python
 alerts_controller = client.alerts
```

### <a name="get_active_alerts"></a>![Method: ](https://apidocs.io/img/method.png ".AlertsController.get_active_alerts") get_active_alerts

> Gets active alerts for multiple devices.

```python
def get_active_alerts(self,
                          device_ids,
                          rule_ids=None,
                          fields=None,
                          sort_by=None,
                          sort_asc=False,
                          page_size='50',
                          page='1')
```

#### Parameters

| Parameter | Tags | Description |
|-----------|------|-------------|
| deviceIds |  ``` Required ```  ``` Collection ```  | What devices to get alerts for. |
| ruleIds |  ``` Optional ```  ``` Collection ```  | Which rules to get alerts for. The default is all rules. |
| fields |  ``` Optional ```  ``` Collection ```  | Which fields to include in the response. Valid fields are those defined in the AlertItem schema. By default all fields are included. |
| sortBy |  ``` Optional ```  ``` Collection ```  | Which fields to sort the response items by. By default the items are sorted by timestampStart. |
| sortAsc |  ``` Optional ```  ``` DefaultValue ```  | Whether to sort the items in ascending order. |
| pageSize |  ``` Optional ```  ``` DefaultValue ```  | The number of items to return per page. |
| page |  ``` Optional ```  ``` DefaultValue ```  | Which page to return when the number of items exceed the page size. |



#### Example Usage

```python
device_ids_value = "[1,2,3]"
device_ids = json.loads(device_ids_value)
rule_ids_value = "[1,2,3]"
rule_ids = json.loads(rule_ids_value)
fields_value = '["ruleId","timestampStart"]'
fields = json.loads(fields_value)
sort_by = ['sortBy']
sort_asc = False
page_size = '50'
page = '1'

result = alerts_controller.get_active_alerts(device_ids, rule_ids, fields, sort_by, sort_asc, page_size, page)

```


### <a name="get_alerts"></a>![Method: ](https://apidocs.io/img/method.png ".AlertsController.get_alerts") get_alerts

> Gets alerts for multiple devices and the given time period.

```python
def get_alerts(self,
                   device_ids,
                   timestamp_start,
                   timestamp_end,
                   rule_ids=None,
                   fields=None,
                   sort_by=None,
                   sort_asc=False,
                   page_size='50',
                   page='1')
```

#### Parameters

| Parameter | Tags | Description |
|-----------|------|-------------|
| deviceIds |  ``` Required ```  ``` Collection ```  | What devices to get alerts for. |
| timestampStart |  ``` Required ```  | The first timestamp to get data for. |
| timestampEnd |  ``` Required ```  | The last timestamp to get data for. |
| ruleIds |  ``` Optional ```  ``` Collection ```  | Which rules to get alerts for. The default is all rules. |
| fields |  ``` Optional ```  ``` Collection ```  | Which fields to include in the response. Valid fields are those defined in the AlertItem schema. By default all fields are included. |
| sortBy |  ``` Optional ```  ``` Collection ```  | Which fields to sort the response items by. By default the items are sorted by timestampStart. |
| sortAsc |  ``` Optional ```  ``` DefaultValue ```  | Whether to sort the items in ascending order. |
| pageSize |  ``` Optional ```  ``` DefaultValue ```  | The number of items to return per page. |
| page |  ``` Optional ```  ``` DefaultValue ```  | Which page to return when the number of items exceed the page size. |



#### Example Usage

```python
device_ids_value = "[1,2,3]"
device_ids = json.loads(device_ids_value)
timestamp_start = 2020-01-01T00:00:00.000Z
timestamp_end = 2020-01-08T00:00:00.000Z
rule_ids_value = "[1,2,3]"
rule_ids = json.loads(rule_ids_value)
fields_value = '["ruleId","timestampStart"]'
fields = json.loads(fields_value)
sort_by = ['sortBy']
sort_asc = False
page_size = '50'
page = '1'

result = alerts_controller.get_alerts(device_ids, timestamp_start, timestamp_end, rule_ids, fields, sort_by, sort_asc, page_size, page)

```


### <a name="get_active_alarms"></a>![Method: ](https://apidocs.io/img/method.png ".AlertsController.get_active_alarms") get_active_alarms

> This endpoint has been deprecated. Please use /activealerts.json.

```python
def get_active_alarms(self,
                          device_ids,
                          rule_ids=None,
                          fields=None,
                          sort_by=None,
                          sort_asc=False,
                          page_size='50',
                          page='1')
```

#### Parameters

| Parameter | Tags | Description |
|-----------|------|-------------|
| deviceIds |  ``` Required ```  ``` Collection ```  | What devices to get alerts for. |
| ruleIds |  ``` Optional ```  ``` Collection ```  | Which rules to get alerts for. The default is all rules. |
| fields |  ``` Optional ```  ``` Collection ```  | Which fields to include in the response. Valid fields are those defined in the AlertItem schema. By default all fields are included. |
| sortBy |  ``` Optional ```  ``` Collection ```  | Which fields to sort the response items by. By default the items are sorted by timestampStart. |
| sortAsc |  ``` Optional ```  ``` DefaultValue ```  | Whether to sort the items in ascending order. |
| pageSize |  ``` Optional ```  ``` DefaultValue ```  | The number of items to return per page. |
| page |  ``` Optional ```  ``` DefaultValue ```  | Which page to return when the number of items exceed the page size. |



#### Example Usage

```python
device_ids_value = "[1,2,3]"
device_ids = json.loads(device_ids_value)
rule_ids_value = "[1,2,3]"
rule_ids = json.loads(rule_ids_value)
fields_value = '["ruleId","timestampStart"]'
fields = json.loads(fields_value)
sort_by = ['sortBy']
sort_asc = False
page_size = '50'
page = '1'

result = alerts_controller.get_active_alarms(device_ids, rule_ids, fields, sort_by, sort_asc, page_size, page)

```


### <a name="get_alarms"></a>![Method: ](https://apidocs.io/img/method.png ".AlertsController.get_alarms") get_alarms

> This endpoint has been deprecated. Please use /alerts.json.

```python
def get_alarms(self,
                   device_ids,
                   timestamp_start,
                   timestamp_end,
                   rule_ids=None,
                   fields=None,
                   sort_by=None,
                   sort_asc=False,
                   page_size='50',
                   page='1')
```

#### Parameters

| Parameter | Tags | Description |
|-----------|------|-------------|
| deviceIds |  ``` Required ```  ``` Collection ```  | What devices to get alerts for. |
| timestampStart |  ``` Required ```  | The first timestamp to get data for. |
| timestampEnd |  ``` Required ```  | The last timestamp to get data for. |
| ruleIds |  ``` Optional ```  ``` Collection ```  | Which rules to get alerts for. The default is all rules. |
| fields |  ``` Optional ```  ``` Collection ```  | Which fields to include in the response. Valid fields are those defined in the AlertItem schema. By default all fields are included. |
| sortBy |  ``` Optional ```  ``` Collection ```  | Which fields to sort the response items by. By default the items are sorted by timestampStart. |
| sortAsc |  ``` Optional ```  ``` DefaultValue ```  | Whether to sort the items in ascending order. |
| pageSize |  ``` Optional ```  ``` DefaultValue ```  | The number of items to return per page. |
| page |  ``` Optional ```  ``` DefaultValue ```  | Which page to return when the number of items exceed the page size. |



#### Example Usage

```python
device_ids_value = "[1,2,3]"
device_ids = json.loads(device_ids_value)
timestamp_start = 2020-01-01T00:00:00.000Z
timestamp_end = 2020-01-08T00:00:00.000Z
rule_ids_value = "[1,2,3]"
rule_ids = json.loads(rule_ids_value)
fields_value = '["ruleId","timestampStart"]'
fields = json.loads(fields_value)
sort_by = ['sortBy']
sort_asc = False
page_size = '50'
page = '1'

result = alerts_controller.get_alarms(device_ids, timestamp_start, timestamp_end, rule_ids, fields, sort_by, sort_asc, page_size, page)

```


[Back to List of Controllers](#list_of_controllers)



